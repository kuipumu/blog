---
date: 2020-08-27 03:30:00
cover: images/uploads/4385953133_b3b5cc5a19_b.jpg 
caption: Circuit Bending Max/MSP, Diana Eng's Fairytale Fashion Show at Eyebeam NYC / 20100224.7D.03474.P1.L1.C45.BW / SML" por See-ming Lee (SML) esta bajo la licencia CC BY-SA 2.0"
title: El uso de algoritmos y computadoras para crear música.
subtitle: Ninguna máquina puede reemplazar el proceso artístico de crear música, pero la máquina puede ayudarnos a emprender un viaje de programación y descubrimiento musical.
categories:
  - Articles
tags:
  - Computación
  - Música
  - Algoritmos
  - Composición
description: "Ninguna máquina puede reemplazar el proceso artístico de crear música, pero la máquina puede ayudarnos a emprender un viaje de programación de descubrimiento musical. Hacer que una computadora cree o ejecute de manera autónoma expresiones artísticas es una tarea que aún hoy en día es muy desafiante, no solo por la complejidad que requiere tales tareas, sino también por las limitaciones de entender cómo interactuar con una máquina para computar tales ideas."
---

La precursora de las computadoras, Ada Lovelace, escribió: "Suponiendo, por ejemplo, que las relaciones fundamentales del sonido agudo en los signos de armonía y de composición musical fueran susceptibles de tal expresión y adaptaciones, el motor podría componer piezas musicales elaboradas y científicas de cualquier tipo, grado de complejidad o extensión ".[^1] Lovelace predijo que la computadora reuniría a científicos y compositores para construir tales piezas musicales a partir de nuevos métodos de programación algorítmica.

Hacer que una computadora cree o ejecute de manera autónoma expresiones artísticas es una tarea que aún hoy en día es muy desafiante, no solo por la complejidad que requiere tales tareas, sino también por las limitaciones de entender cómo interactuar con una máquina para computar tales ideas. Múltiples artistas en las últimas décadas han trabajado para lograr estas tareas a través de muchas herramientas, teorías y mecanismos diferentes, como la traducción de texto a música [^2], ecuaciones matemáticas y eventos aleatorios, argumentos prefabricados que se utilizan para componer nuevas obras basadas en composiciones existentes [^3], como lenguaje con un conjunto gramatical distintivo, algoritmos genéticos [^4], auto-aprendizaje [^5] y métodos híbridos utilizando todos los métodos anteriores.

Aquí hay una lista de algunos artistas y las diferentes creaciones, enfoque y mecanismos, que utilizaron para crear música en computadoras usando algoritmos.

---

### Lejaren Hiller y los algoritmos de Markov Chainsi

{{< youtube v5z3t2qz4qo >}}

Lejaren Hiller fue el primer compositor que utilizo software de computadora para crear una composición usando algoritmos, en colaboración con Leonard Isaacson, de 1955 a 1956 con la computadora ILLIAC I en la Universidad de Illinois en Urbana–Champaign. Su trabajo resultó en la primera composición asistida por computadora conocida, The Illiac Suite for String Quartet. Programado en binario y utilizando, entre otras técnicas, los algoritmos de generación de tono de "recorrido aleatorio" de Markov Chainsi. [^6]

---

## Iannis Xenakis y el SMP (Programa de Música Estocástica)

{{< youtube nvH2KYYJg-o >}}

Iannis Xenakis fue un pionero en la composición algorítmica y la música por computadora, escribió: “Con la ayuda de computadoras electrónicas, el compositor se convierte en una especie de piloto: presiona botones, introduce coordenadas y supervisa los controles de una nave cósmica que navega en el espacio de sonido, a través de constelaciones sónicas y galaxias que antes sólo podía vislumbrar en un sueño distante.”[^7] El enfoque de Xenakis, que condujo al Programa de Música Estocástica (SMP), utilizó fórmulas desarrolladas originalmente por científicos para explicar el comportamiento de las partículas de gas.[^8] La elección y distribución de notas se determinó mediante procedimientos de elección aleatoria, tablas de probabilidad que ponderan la ocurrencia de eventos específicos contra los de otros. Nouritza Matossian, una escritora de música contemporánea, escribió: "Con un solo programa de 45 minutos en el IBM 7090, logró producir no solo ocho composiciones que se destacan como obras integrales, sino también liderar el desarrollo de la composición asistida por computadora".[^8]

---

## David Cope y el EMI (Experimentos en Inteligencia Musical)

{{< youtube CgG1HipAayU >}}

David Cope creó en 1981 el EMI (Experiments in Musical Intelligence) mientras sufría un caso frustrante de bloqueo del compositor, dijo en una entrevista “Experiments in Musical Intelligence es un programa de análisis que utiliza su salida para componer nuevos ejemplos de música en el estilo de la música en su base de datos sin replicar ninguna de esas piezas exactamente."[^9] El programa analizaría la música que Cope había ingresado en la base de datos de EMI, y esa entrada se usaría para dirigir la composición de nuevas obras en el mismo estilo.[^9] Años más tarde, en 2009, lanzó Emily Howell, una sucesora de EMI, dijo: “Puse mis habilidades en crear un compositor único que creara música clásica contemporánea que sería interesante y captaría la atención de la gente, pero que no tenía el estilo de nadie excepto ese tipo de software en particular."[^10]

---

## George Lewis y el lenguaje de programación Forth

{{< youtube hO47LiHsFtc >}}

George Lewis creó Voyageris, una herramienta para la improvisación musical o como él llamó una "orquesta virtual de improvisación interactiva, impulsada por computadora".[^11] Sus raíces están, según Lewis, en la tradición afroamericana de dominio múltiple, descrito por él como como la involucración de múltiples flujos estructurales simultáneos, siendo estos "tanto la estructura lógica del software como su articulación de desempeño". La Voyager fue programada en el idioma Forth. En Voyager, la computadora se usa para analizar y responder a un humano, tal entrada no es esencial para que el programa genere música. Lewis escribió: "Concibo una interpretación de Voyager como múltiples corrientes paralelas de generación de música, que emanan tanto de las computadoras como de los humanos un modelo de discurso no jerárquico, improvisado, sujeto-sujeto, en lugar de una configuración de estímulo/respuesta".[^11]

---

## Brian Eno y el software SSEYO Koan Pro

{{< youtube VvHcnS-popM >}}

Brian Eno es conocido por acuñar el término música generativa, lo utilizó en álbumes como Ambient 1 (Music For Airports). Eno dijo sobre su álbum Discreet Music, “Como siempre he preferido hacer planes a ejecutarlos, he gravitado hacia situaciones y sistemas que, una vez puestos en funcionamiento, podrían crear música con poca o ninguna intervención de mi parte. Es decir, tiendo hacia los roles de planificador y programador, y luego me convierto en una audiencia de los resultados.”[^12]

En 1995 comenzó a trabajar con el software Koan Pro de SSEYO, lo que lo llevó a la publicación de su título Generative Music 1. Koan fue el primer sistema de generación de música en tiempo real de SSEYO, desarrollado para Microsoft Windows.[^13]

---

## Gee Wang y el lenguaje de programación ChucK

{{< youtube chA-4GRCb-I >}}

Ge Wang es el autor y arquitecto principal del lenguaje de programación musical ChucK.[^14] ChucK es un lenguaje de programación para la síntesis de sonido en tiempo real y la creación musical. ChucK presenta un modelo de programación simultáneo basado en el tiempo único que es preciso y expresivo, tasas de control dinámico y la capacidad de agregar y modificar código sobre la marcha.[^15]

---

## Alex McLean y el software TidalCycles

{{< youtube dIpzU71LAQQ >}}

Alex McLean se destaca por su papel clave en el desarrollo de la codificación en vivo como práctica musical, incluida la creación de TidalCycles, un software que permite a los músicos programadores codificar música.[^16] TidalCycles es un software que te permite crear patrones con código, incluye lenguaje para describir secuencias flexibles (por ejemplo, polifónicas, polirrítmicas, generativas). También tiene una extensa biblioteca de funciones de creación de patrones, para transformarlas y combinarlas. [^ 16]

---

## Sam Aaron y el software Sonic Pi

{{< youtube G1m0aX9Lpts >}}

Sam Aaron, un arquitecto e investigador de software[^17], en colaboración con la Fundación Raspberry Pi en el Laboratorio de Computación de la Universidad de Cambridge, desarrolló Sonic Pi, un entorno de codificación en vivo basado en Ruby, originalmente diseñado para admitir lecciones de computación y música en las escuelas, el uso del motor de síntesis SuperCollider también se usa para codificación en vivo y otras formas de interpretación y producción musical algorítmica. [^ 18]

---

### Scott (accreil) y el software Pure Data

{{< youtube n9C58n7FC5c >}}

Scott (accreil) es un artista que trabaja principalmente en realizar ingenieria inversa en dispositivos y el hackeo de dispositivos con chips DSP.[^20] Ha trabajado en varios álbumes y pistas que ha publicado en Bandcamp, algunas de estas pistas se han compuesto y ejecutado utilizando solo un software llamado Pure Data. Pure Data es un entorno de programación gráfica en tiempo real para procesamiento de audio, video y gráficos. Es la tercera rama importante de la familia de lenguajes de programación de patcher conocidos como Max (Max / FTS, ISPW Max, Max / MSP, jMax, etc.) desarrollado originalmente por Miller Puckette y compañía en IRCAM (Institut de Recherche et Coordination Acoustique/Musique). Pure Data es básicamente un software que permite al usuario tomar datos en su estado puro y usarlos en tiempo real para interactuar con otros objetos usando su entorno de flujo de datos.[^21]

---

## Referencias

[^1]: Alpern, A. (1995). Techniques for algorithmic composition of music. http://alum.hampshire.edu/~adaF92/algocomp/algocomp95.html. Hampshire College. 

[^2]: Davis, H. (2014). Generating Music from Literature. Proceedings of the EACL Workshop on Computational Linguistics for Literature: 1–10. arXiv:1403.2124. Bibcode:2014arXiv1403.2124D. doi:10.3115/v1/W14-0901.

[^3]: Brown, S. (1997). Algorithmic Composition and Reductionist Analysis: Can a Machine Compose?. Cambridge University New Music Society.

[^4]: Fox, C. (2006). Genetic Hierarchical Music Structures. American Association for Artificial Intelligence.

[^5]: S. Dubnov, G. Assayag, O. Lartillot, G. Bejerano, (2003, Octubre). "Using Machine-Learning Methods for Musical Style Modeling". IEEE Computers. 36 (10). pp. 73-80.

[^6]: Hiller, L. A. H., & Isaacson, L. M. (1959). Experimental Music: Composition With an Electronic Computer (2nd ed.). McGraw-Hill.

[^7]: Xenakis, I. (1992). Formalized Music. Pendragon, Hillsdale, New York.

[^8]: Matossian, N. and Xenakis, I. (1986). Kahn & Averill. London.

[^9]: Garcia, C. (2019, Septiembre 24). Algorithmic Music – David Cope and EMI. CHM. https://computerhistory.org/blog/algorithmic-music-david-cope-and-emi/

[^10]: Leach, B. (2009, Octubre 22). Emily Howell: the computer program that composes classical music. Telegraph.Co.Uk. https://www.telegraph.co.uk/culture/music/music-news/6404737/Emily-Howell-the-computer-program-that-composes-classical-music.html

[^11]: Lewis, G. (2000). Too many notes: computers, complexity, and culture in Voyager. Leonardo Music Journal 10 (2000), 33–39. 

[^12]: Holmes, T. (2003). Electronic and Experimental Music. Taylor & Francis Ltd., London.

[^13]: Cole, T. C. A. P. (2019). Generative Music & Eno’s GM1 w/ SSEYO Koan (now WOTJA) Software. Intermorphic Ltd. https://intermorphic.com/sseyo/koan/generativemusic1/

[^14]: Wang, G. (n.d.). Ge Wang | Bio. Ge Wang. https://www.gewang.com/bio/

[^15]: ChucK => Strongly-timed, On-the-fly Music Programming Language. (n.d.). ChucK : Strongly-Timed, Concurrent, and On-the-Fly Music Programming Language. Obtenido el 21 de agosto, 2020, from http://chuck.stanford.edu/

[^16]: TidalCycles. (n.d.). TidalCycles. Obtenido el 27 de agosto del 2020, from https://tidalcycles.org/index.php/Welcome

[^17]: Aaron, S. (n.d.). Sam Aaron - A Communicative Programmer. Sam Aaron - A Communicative Programmer. Obtenido el 27 de agosto del 2020, from http://sam.aaron.name/

[^18]: Sonic Pi - The Live Coding Music Synth for Everyone. (n.d.). Sonic Pi - The Live Coding Music Synth for Everyone. Obtenido el 27 de agosto del 2020, De https://sonic-pi.net/

[^19]: Interview with Alex McLean. (2018, April 29). Esoteric.Codes. https://esoteric.codes/blog/interview-with-alex-mclean

[^20]: About. (2018, May 21). Acreil. https://acreil.wordpress.com/about/

[^21]: About Pure Data. (2007). Pd Community Site. Obtenido el 27 de agosto del 2020, from https://puredata.info/about/index_html
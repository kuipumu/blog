---
title: "Acerca"
date: 2020-08-27 02:30:00
cover: images/uploads/11742728_10207237192596974_6728112836139156504_n.jpg
---
Hola, soy César Montilla, soy desarrollador web, diseñador, administrador de sistemas, autodidacta, entusiasta de la tecnología, amante de la música y los videojuegos.

---
date: 2020-08-27 03:30:00
cover: images/uploads/4385953133_b3b5cc5a19_b.jpg
caption: Circuit Bending Max/MSP, Diana Eng's Fairytale Fashion Show at Eyebeam NYC / 20100224.7D.03474.P1.L1.C45.BW / SML" by See-ming Lee (SML) is licensed under CC BY-SA 2.0"
title: The use of algorithms and computers to create music.
subtitle: No machine can replace the artistic process of making music, but the machine can help us take a programming journey of musical discovery
categories:
  - Articles
tags:
  - Computer
  - Music
  - Algorithm
  - Composition
description: "No machine can replace the artistic process of making music, but the machine can help us take a programming journey of musical discovery. Making a computer autonomously create or execute artistic expressions is a task that even to this day is a very challenging one, not only because of the complexity that requires such tasks, but also because of the limitations of understanding how to interact with a machine to compute such ideas."
---

The precursor of computers Ada Lovelace, wrote "Supposing, for instance, that the fundamental relations of pitched sound in the signs of harmony and of musical composition were susceptible of such expression and adaptations, the engine might compose elaborate and scientific pieces of music of any degree of complexity or extent."[^1] Lovelace predicted, the computer brought scientists and composers together to construct such pieces of music out of new algorithmic programming methods.

Making a computer autonomously create or execute artistic expressions is a task that even to this day is a very challenging one, not only because of the complexity that requires such tasks, but also because of the limitations of understanding how to interact with a machine to compute such ideas. Multiple artists over the last few decades have worked on achieving this tasks over many different tools, theories and mechanisms, such has translating text into music[^2], mathematical equations and random events, pre-made arguments used to compose new works based on existing compositions[^3], as a language with a distinctive grammar set, genetic algorithms[^4], self-learning[^5] and hybrid methods with all of the previous methods. 

Here is a list of a few artists and their different creations and approaches 
to use algorithms in computers to create music.

---

## Lejaren Hiller and the Markov Chainsi algorithms

{{< youtube v5z3t2qz4qo >}}

Lejaren Hiller was the first composer to have applied computer software to create a composition using algorithms, in collaboration with Leonard Isaacson, from 1955 to 1956 with the ILLIAC I computer at the University of Illinois at Urbana–Champaign. His work resulted in the first known computer aided composition, The Illiac Suite for String Quartet. Programmed in binary, and using, among other techniques, Markov Chainsi "random walk" pitch-generation algorithms.[^6]

---

## Iannis Xenakis and the SMP (Stochastic Music Programme)

{{< youtube nvH2KYYJg-o >}}

Iannis Xenakis was a pioneer on algorithmic composition and computer music, he wrote, “With the aid of electronic computers, the composer becomes a sort of pilot: he presses buttons, introduces coordinates, and supervises the controls of a cosmic vessel sailing in the space of sound, across sonic constellations and galaxies that he could formerly glimpse only in a distant dream.”[^7] Xenakis’s approach, led to the Stochastic Music Programme, he used formulas originally developed by scientists to explain the behavior of gas particles.[^8] The choice and distribution of notes was determined by procedures involving random choice, probability tables weighing the occurrence of specific events against those of others. Nouritza Matossian, a contemporary music writer wrote, “With a single 45-minute program on the IBM 7090, he succeeded in producing not only eight compositions that stand up as integral works but also in leading the development of computer aided composition.”[^8]

---

## David Cope and the EMI (Experiments in Musical Intelligence)

{{< youtube CgG1HipAayU >}}

David Cope created in 1981 the EMI (Experiments in Musical Intelligence) while suffering from a frustrating case of composer's block, he said in an interview “Experiments in Musical Intelligence is an analysis program that uses its output to compose new examples of music in the style of the music in its database without replicating any of those pieces exactly"[^9]. The program would analyze music Cope had entered enter into EMI’s database, and that input would be used to direct the composition of new works in the same style.[^9] Years later in 2009, he released Emily Howell, a successor to EMI, he said “I put my skills into creating a unique composer that created contemporary classical music that would be interesting and grab people’s attention, but was in nobody’s style except that particular kind of software.”[^10]

---

## George Lewis and the Forth programming language

{{< youtube hO47LiHsFtc >}}

George Lewis’s created Voyageris, a tool for musical improvisation or how he called a “computer-driven, interactive ‘virtual improvising orchestra.’”[^11] Its roots are, according to Lewis, in the African-American tradition of multi-dominance, described by him as involving multiple simultaneous structural streams, these being “both the logical structure of the software and its performance articulation.” Voyager was programmed in the Forth language. In Voyager the computer is used to analyze and respond to a human, such input is not essential for the program to generate music. Lewis wrote, “I conceive a performance of Voyager as multiple parallel streams of music generation, emanating from both the computers and the humans a non hierarchical, improvisational, subject-subject model of discourse, rather than a stimulus/response setup.”[^11]

---

## Brian Eno and SSEYO Koan Pro software

{{< youtube VvHcnS-popM >}}

Brian Eno is known for coining the term generative music, he used it albums like Ambient 1 (Music For Airports). Eno said about his album Discreet Music, “Since I have always preferred making plans to executing them, I have gravitated towards situations and systems that, once set into operation, could create music with little or no intervention on my part. That is to say, I tend towards the roles of planner and programmer, and then become an audience to the results.”[^12]

In 1995 he started working with SSEYO's Koan Pro software, which led him to the publication of his title Generative Music 1. Koan was SSEYO's first real-time music generation system, developed for Microsoft Windows.[^13]

---

## Gee Wang and the ChucK programming language

{{< youtube chA-4GRCb-I >}}

Ge Wang is the author and chief architect of the ChucK music programming language.[^14] ChucK is a programming language for real-time sound synthesis and music creation. ChucK presents a unique time-based, concurrent programming model that's precise and expressive, dynamic control rates, and the ability to add and modify code on-the-fly.[^15]

---

## Alex McLean and the TidalCycles software

{{< youtube dIpzU71LAQQ >}}

Alex McLean is notable for his key role in developing live coding as a musical practice, including for creating TidalCycles, a software that allows programmer musicians to code music.[^16] TidalCycles is a software, that allows you to make patterns with code, it includes language for describing flexible (e.g. polyphonic, polyrhythmic, generative) sequences. It also has an extensive library of patterning functions, for transforming and combining them.[^16]

---

## Sam Aaron and the Sonic Pi software

{{< youtube G1m0aX9Lpts >}}

Sam Aaron a software architect and researcher[^17] in collaboration with the Raspberry Pi Foundation on the University of Cambridge Computer Laboratory developed Sonic Pi, a live coding environment based on Ruby, originally designed to support both computing and music lessons in schools, its use of the SuperCollider synthesis engine is also used for live coding and other forms of algorithmic music performance and production.[^18]

---

### Scott (accreil) and the Pure Data software

{{< youtube n9C58n7FC5c >}}

Scott (accreil) is a artist that works mostly on reverse engineer electronic devices and hack devices with DSP chips.[^20] He has worked on multiple albums and tracks that have been published on Bandcamp, some of these tracks have been composed and executed using only a software called Pure Data. Pure Data is a real-time graphical programming environment for audio, video, and graphical processing. It is the third major branch of the family of patcher programming languages known as Max (Max/FTS, ISPW Max, Max/MSP, jMax, etc.) originally developed by Miller Puckette and company at IRCAM (Institut de Recherche et Coordination Acoustique/Musique). Pure Data is basically a software that let's the user take data in it's pure state and use it in real time to interact with other objects using is dataflow enviroment.[^21]

---

## References

[^1]: Alpern, A. (1995). Techniques for algorithmic composition of music. http://alum.hampshire.edu/~adaF92/algocomp/algocomp95.html. Hampshire College. 

[^2]: Davis, H. (2014). Generating Music from Literature. Proceedings of the EACL Workshop on Computational Linguistics for Literature: 1–10. arXiv:1403.2124. Bibcode:2014arXiv1403.2124D. doi:10.3115/v1/W14-0901.

[^3]: Brown, S. (1997). Algorithmic Composition and Reductionist Analysis: Can a Machine Compose?. Cambridge University New Music Society.

[^4]: Fox, C. (2006). Genetic Hierarchical Music Structures. American Association for Artificial Intelligence.

[^5]: S. Dubnov, G. Assayag, O. Lartillot, G. Bejerano, (2003, October). "Using Machine-Learning Methods for Musical Style Modeling". IEEE Computers. 36 (10). pp. 73-80.

[^6]: Hiller, L. A. H., & Isaacson, L. M. (1959). Experimental Music: Composition With an Electronic Computer (2nd ed.). McGraw-Hill.

[^7]: Xenakis, I. (1992). Formalized Music. Pendragon, Hillsdale, New York.

[^8]: Matossian, N. and Xenakis, I. (1986). Kahn & Averill. London.

[^9]: Garcia, C. (2019, September 24). Algorithmic Music – David Cope and EMI. CHM. https://computerhistory.org/blog/algorithmic-music-david-cope-and-emi/

[^10]: Leach, B. (2009, October 22). Emily Howell: the computer program that composes classical music. Telegraph.Co.Uk. https://www.telegraph.co.uk/culture/music/music-news/6404737/Emily-Howell-the-computer-program-that-composes-classical-music.html

[^11]: Lewis, G. (2000). Too many notes: computers, complexity, and culture in Voyager. Leonardo Music Journal 10 (2000), 33–39. 

[^12]: Holmes, T. (2003). Electronic and Experimental Music. Taylor & Francis Ltd., London.

[^13]: Cole, T. C. A. P. (2019). Generative Music & Eno’s GM1 w/ SSEYO Koan (now WOTJA) Software. Intermorphic Ltd. https://intermorphic.com/sseyo/koan/generativemusic1/

[^14]: Wang, G. (n.d.). Ge Wang | Bio. Ge Wang. https://www.gewang.com/bio/

[^15]: ChucK => Strongly-timed, On-the-fly Music Programming Language. (n.d.). ChucK : Strongly-Timed, Concurrent, and On-the-Fly Music Programming Language. Retrieved August 27, 2020, from http://chuck.stanford.edu/

[^16]: TidalCycles. (n.d.). TidalCycles. Retrieved August 27, 2020, from https://tidalcycles.org/index.php/Welcome

[^17]: Aaron, S. (n.d.). Sam Aaron - A Communicative Programmer. Sam Aaron - A Communicative Programmer. Retrieved August 27, 2020, from http://sam.aaron.name/

[^18]: Sonic Pi - The Live Coding Music Synth for Everyone. (n.d.). Sonic Pi - The Live Coding Music Synth for Everyone. Retrieved August 27, 2020, from https://sonic-pi.net/

[^19]: Interview with Alex McLean. (2018, April 29). Esoteric.Codes. https://esoteric.codes/blog/interview-with-alex-mclean

[^20]: About. (2018, May 21). Acreil. https://acreil.wordpress.com/about/

[^21]: About Pure Data. (2007). Pd Community Site. Retrieved August 27, 2020, from https://puredata.info/about/index_html